import java.util.Random;


public class ApplianceModel {
	static Random generator;
	static int appRange = 5;
	static final String generateNextState(String states, long seed){
		String next = "";
		int changeDigit = 0;
		generator = new Random(seed);
		//hangeDigit = (int) Math.floor(generator.nextDouble()*5);
		changeDigit = (int) Math.floor(Math.random() * appRange);
//		while(changeDigit == 2)
//			changeDigit = (int) Math.floor(Math.random() * appRange);
		for(int i = 0; i < states.length(); i++){
			if(i == changeDigit){
				if(states.charAt(i) == '0')
					next = next + '1';
				else 
					next = next + '0';
			}else{
				next = next + states.charAt(i);
			}
		}
		//return next;
		return states+ "_" + next;
				
	}
}
