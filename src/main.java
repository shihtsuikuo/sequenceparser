import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.InputStreamReader;
import java.util.LinkedList;
import java.util.Queue;
import java.util.StringTokenizer;

import SignaturesDisaggregate.Complex;
//import SignaturesDisaggregate.*;
import SignaturesDisaggregate.FFT;

/*
 *  main.java
 *  
 *  This program does 3 things:
 *
 *	1. generate sequence randomly
 *	2. open corresponding file for this random sequence
 *	3. calculate the features(Vrms, Irms, P, Q) and write to other files.
 *
 */
public class main {
	/** usage: printHex(byte_array);*/
	public static void printHex(byte[] bytes){
		StringBuilder sb = new StringBuilder();
	    for (byte b : bytes) {
	        sb.append(String.format("%02X ", b));
	    }
	    System.out.println("hex: " + sb.toString());
	}
	
	/** usage: littleEndian(byte_array, length_of_array) */
	public static void littleEndian(byte[] bytes, int len){
		for(int i = 0; i < len/2; i++){
			byte t = bytes[i];
			bytes[i] = bytes[len-1-i];
			bytes[len-1-i] = t;
		}
	}
	private static int findClose2Power(int data_size) {
		int return_value = 1;
		while(data_size/2 > 0){ 
			return_value*=2;
			data_size /= 2;
		}
		return return_value;
	}

	
	public static void main(String[] args) {
		final int SEQUENCE_LENGTH = 60 * 60;//1 * 60 * 60 (second)
		final int APPLIANCE_NUM = 5;
		final int OUTPUT_FILE_SIZE = 10 * 60; // 10 minutes a file, 10 * 60 seconds
		final int window_size = 128;     //calculate features for each 256 sample
		final int downsampling_rate = 8; //if 1, do not down-sample this time.
		final int original_sampling_rate = 1000; // PSpice sample rate 1000Hz
		final float harmonic_unit = (float)original_sampling_rate/(float)(window_size*downsampling_rate);
		final float phase_error = (float) Math.PI / 2;
		boolean twice = false;
		final String[] source_files = { "00000",  "00000_00100", "00100", "00100_00000","00000",  "00000_00100", "00100", "00100_00000","00000",  "00000_00100", "00100", "00100_00000","00000",  "00000_00100", "00100", "00100_00000","00000",  "00000_00100", "00100", "00100_00000","00000"};
		String filename = "00000";
//		String filename = "KNNtest2";
		int allFileTimeCount = 0;
		float accurate_timestamp = 0.0F, last_timestamp = 2.0F;
		int file_count = 0;
		int sample = 0; //# of samples in one cycle
		float voltage_cycle_start = 0.0F, current_cycle_start = 0.0F; //the starting point of cycle (present with index of sample)
		float old_cycle_start = 0;
		String inputFile_current, inputFile_voltage;
//		String outputFile = "F:\\brass lab\\Thesis\\experiment\\Pspice Sequence\\features_KNNtestplz4";
//		String outputFile = "F:\\brass lab\\Thesis\\experiment\\Pspice Event\\features_3840Hz.txt";
		String outputFile = "F:\\brass lab\\Thesis\\experiment\\Pspice Sequence\\NotThatConfused\\Sequence_2\\features.txt";
		float lastValue_current = 0.0F, lastValue_voltage = 0.0F;
		Queue<Float> store_voltage = new LinkedList<Float>(); 
		Queue<Float> store_current = new LinkedList<Float>(); 
		float value_current = 0, value_voltage = 0;
		float amp_current = 0, amp_voltage = 0;
		float delta_phase = 0.0F;
		float tempP = 0.0F;
		float tempQ = 0.0F;
		float tempI = 0.0F;
		float tempV = 0.0F;
		InputStreamReader inputStreamReader_current, inputStreamReader_voltage;
		FileWriter fileOutputStream;
		
		try {
			
			fileOutputStream = new FileWriter(outputFile, true);
			
			while(file_count < SEQUENCE_LENGTH / 5){   // 5 seconds in a file
//			while(file_count < source_files.length){   // 5 seconds in a file
//				filename = source_files[file_count];

				System.out.println("Read file: " + filename);
				/* open file*/
//				inputFile_current = "C:\\Users\\Ariel_K\\Dropbox\\share_files\\NILM testdata\\3840Hz.txt";
				inputFile_current = "F:\\brass lab\\Thesis\\experiment\\Pspice Event\\NotThatConfused\\" + filename + ".txt";
				
				inputStreamReader_current = new InputStreamReader(new FileInputStream(inputFile_current));
				
				lastValue_voltage = 0;
				lastValue_current = 0;
				sample = 0;
				
				BufferedReader br_current = new BufferedReader(inputStreamReader_current);
				String rdLine;
				int GG = 0;
				rdLine = br_current.readLine();
				while ((rdLine = br_current.readLine()) != null){
				//while ((line_voltage = br_voltage.readLine()) != null  && (line_current = br_current.readLine()) != null && GG++ < 300){
					
					/****** handle voltage first *****/
					/* get data */
					StringTokenizer tkn = new StringTokenizer(rdLine);
					float current_timestamp = Float.parseFloat((String) tkn.nextElement());
					if(Math.floor(last_timestamp) < Math.floor(current_timestamp)) allFileTimeCount++;
					accurate_timestamp = current_timestamp % 1;
					value_voltage = Float.parseFloat((String) tkn.nextElement());
					if(sample % downsampling_rate == 0)store_voltage.offer(value_voltage);
					if(store_voltage.size() > window_size) store_voltage.poll();
					//System.out.println((allFileTimeCount + accurate_timestamp));
					/* a new cycle*/
					if(lastValue_voltage < 0 && value_voltage >= 0){  
						amp_voltage = 0;
						if(voltage_cycle_start == 0.0F){
							voltage_cycle_start = ZeroInterpolation(last_timestamp, current_timestamp, lastValue_voltage, value_voltage);
						}
					}
					
					lastValue_voltage = value_voltage;
					/* record the amplitude of V */
					if(value_voltage > amp_voltage) amp_voltage = value_voltage;
					
					/****** handle current *****/
					
//					StringTokenizer Tok_c = new StringTokenizer(line_current);
//					Float.parseFloat((String) Tok_c.nextElement());  //ignore time, voltage part has handled it
					value_current = Float.parseFloat((String) tkn.nextElement());
					if(sample % downsampling_rate == 0) store_current.offer(value_current);
					if(store_current.size() > window_size) store_current.poll();
					
					// a new cycle
					if(lastValue_current <= 0.0F && value_current >= 0.0F && lastValue_current*value_current != 0.0F){  
						amp_current = 0;
						if(current_cycle_start == 0.0F){
							current_cycle_start = ZeroInterpolation(last_timestamp, current_timestamp, lastValue_current, value_current);
							
						}
						
					}
					sample++;
					last_timestamp = current_timestamp;
					lastValue_current = value_current;
					// record the amplitude of I
					if(value_current > amp_current) amp_current = value_current;	
					
					//System.out.println(timestamp_str + " " + value_current);
					//if(true) continue;
					
					/******************************************************/
					/**************** calculate P,Q, Harmonices ***********/
					/******************************************************/
					/* check if delta_phase is now available */
					if(voltage_cycle_start != 0 && current_cycle_start != 0){
						/* in most appliance, the current will be lag from voltage*/
						float new_delta_phase = (float)(current_cycle_start-voltage_cycle_start)/(float)(voltage_cycle_start-old_cycle_start);
						if (Math.abs(new_delta_phase-delta_phase) < phase_error){
							delta_phase = new_delta_phase; 
							
							//System.out.println(voltage_cycle_start+" "+current_cycle_start+ " "+old_cycle_start+"=======================================================");
							
							old_cycle_start = voltage_cycle_start;
							voltage_cycle_start = 0.0F;
							current_cycle_start = 0.0F;
						}
					}			
					//System.out.println(delta_phase);
					
					if( (store_voltage.size() == window_size) && (sample % downsampling_rate == 0) ){
						
						tempP = 0.0F;
						tempQ = 0.0F;
						float current_temp = 0;
						float voltage_temp = 0;
						
						int data_size = (store_voltage.size() >= store_current.size()) ? store_current.size() : store_voltage.size();
						int fft_data_size = findClose2Power(data_size);
						Complex[] x_current = new Complex[fft_data_size];
						
						
						for(int d = 0; d < data_size; d++){
							if(d < fft_data_size){
								x_current[d] = new Complex(store_current.peek(), 0);
							}
							current_temp = store_current.poll();
							voltage_temp = store_voltage.poll();
							tempI += current_temp*current_temp;
							tempV += voltage_temp*voltage_temp;
							store_current.offer(current_temp);
							store_voltage.offer(voltage_temp);
						}
						tempI = tempI/window_size;
						tempI = (float) Math.sqrt(tempI);
						tempV = tempV/window_size;
						tempV = (float) Math.sqrt(tempV);
						//if(filename.equals("00000")){
						//	tempP = (float) (tempI*tempV);
						//	tempQ = 0.0F;
						//}else{
							tempP = (float) (tempI*tempV*Math.cos(delta_phase*2*Math.PI));
							tempQ = (float) (tempI*tempV*Math.sin(delta_phase*2*Math.PI));
						//}
						//System.out.println(tempI + " " + tempV + " " + Math.cos(delta_phase*2*Math.PI) + " " + old_cycle_start);
						fileOutputStream.write(String.format("%.5f", (allFileTimeCount + accurate_timestamp)) + " " 
											+ String.format("%.5f", tempI)+ " " + String.format("%.5f", tempV) + " "
											+ String.format("%.5f", tempP)+ " " + String.format("%.8f", tempQ) + "\n");
						fileOutputStream.flush();
						
				        // FFT of original data
				        //Complex[] y_current = FFT.fft(x_current);
				       // fileOutputStream_Harmonics.write(String.valueOf(time) + "."+ attachTimestamp + " ");
				        
				        
				        //   h1 = 60 hz point
						//   h2 = 60x3 hz point
						//   h3 = 60x5 hz point
				        /* 
				        for(int h = 1; h <= 19; h+=2){
				        	Double tempHarmonic = 0.0;
				        	int floor = 0;
				        	int add_half_floor = 0;
				        	float index = 60 * h / harmonic_unit;
				        	System.out.println(harmonic_unit + " " + h);
				        	floor = (int) Math.floor(index);
				        	add_half_floor = (int) Math.floor(index + 0.5F);
				        	if(floor == add_half_floor) 
				        		tempHarmonic = y_current[floor].abs();
				        	else
				        		tempHarmonic = y_current[add_half_floor].abs();
				        	fileOutputStream.write(String.format("%.5f", (allFileTimeCount + accurate_timestamp)) + " " 
									+ String.format("%.5f", tempI)+ " " + String.format("%.5f", tempV) + " "
									+ String.format("%.5f", tempP)+ " " + String.format("%.8f", tempQ) + " ");
				        	
				        	//fileOutputStream_Harmonics.write(String.format("%.3f", tempHarmonic) + " ");
				        	fileOutputStream.write(String.format("%.5f", tempHarmonic) + " ");
				        	if(h == 19){ 
				        		//fileOutputStream_Harmonics.write("\n");
				        		fileOutputStream.write("\n");
				        	}
				        	
				        	fileOutputStream.flush();
				        	//System.out.println("harmonics:" + temp_harmonic.toString());
				        
				        }
				        */
					}
				}//end of reading one file
				sample = 0;
				inputStreamReader_current.close();
				//if(allFileTimeCount % OUTPUT_FILE_SIZE == 0){
					//System.out.println(allFileTimeCount);
					fileOutputStream.flush();
				//	fileOutputStream.close();
				//	fileOutputStream = new FileWriter(outputFile + allFileTimeCount, true);
				//}
				file_count++;
				
				//if(sample==0) break;
				
				if(filename.length() <= APPLIANCE_NUM ){
					if(twice == false){
						twice = true;
					}else{
						twice = false;
						filename = ApplianceModel.generateNextState(filename, System.currentTimeMillis());
					}
					
				}else{
					filename = filename.substring(APPLIANCE_NUM + 1);
					twice = false;
				}
				
				
//				if(file_count < source_files.length)
//					filename = source_files[file_count];
			}	
				
		} catch (FileNotFoundException e) {
			System.out.println("No such file.");
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}	
	}
	
	private static float ZeroInterpolation(float lastTime, float time, float lastValue, float value) {
		float ratio;
		if(lastValue > 0 || value < 0){
			System.out.println("Wrong pass-zero time!!");
			return lastTime;
		}
		ratio = (value-lastValue)/(time-lastTime);
		return ((0-lastValue) / ratio) + lastTime;
	}
	
}
